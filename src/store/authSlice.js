import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isAuth: false,
    user:null,
    token:'$2y$12$PiX.1feLOl1X7FFDo3S6w.Z5yyjPEAoIAWPhaSK2o.PbINCe.tz6q',
}


export const authSlice = createSlice({
    name:'authSlice',
    initialState,
    reducers:{
        setAuth:(state,action)=>
        {
            const {user,token} = action.payload;
            state.user = user;
            state.isAuth = true;
            state.token = token;
        }
    }
})

export const {setAuth} = authSlice.actions;

export default authSlice.reducer;

